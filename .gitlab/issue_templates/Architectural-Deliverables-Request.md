<!--

This template should be used when requesting consideration of new
components or replacing existing components.

The title should reflect the problem to solve, not the solution.

Correct: Consider new loadbalancer solution
Incorrect: Use SuperBalancer9000 instead of SlowBalancer02

-->

## Summary

<!--

Explain the context for the request, for example customer need or
capabilities required for new features and enhancements to the GitLab
application.

-->

## Deliverables

<!--

The selection process is handled by the next issue opened, which should be a
research spike to validate the matrix and figure out the best fit both on
features and integration to GitLab Omnibus and GitLab Cloud Native.

-->

1. Identify what problems our current choices do not solve today.
1. Identify other needs in this space currently on the future product
   roadmap
1. Develop a matrix that plots architectural requirements against features
   of various proposed solutions to enable the selection process

/label ~"group::distribution" ~"devops::enablement" ~"section::enablement" ~"maintainer-discussion"
